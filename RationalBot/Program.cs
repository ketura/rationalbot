﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.API;

namespace RationalBot
{
	public class Program
	{
		static void Main(string[] args)
		{
			ColorBot bot = new ColorBot();
			Console.WriteLine("Beginning connection attempt...");
			var Client = new DiscordClient();

			bot.Init(Client);

			try
			{
				Client.ExecuteAndWait(async () =>
				{
					await Client.Connect(bot.Token, TokenType.Bot);
				});
			}
			catch (Discord.Net.HttpException he)
			{
				Console.WriteLine(string.Format("Credentials provided were not valid for user {0}!", bot.Name));
			}

			Console.ReadLine();
		}
	}
}
