﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;

namespace RationalBot
{
	public class ColorBot : RationalBot
	{
		public ColorBot()
		{
			Name = "ColorBot";
			ClientID = 291645535599984651;
			ClientSecret = "jX2yqnT35uMukj05uzggT-c8HmKolB1_";
			Token = "MjkxNjQ1NTM1NTk5OTg0NjUx.C6simw.Xz33CM1c1qkekE7fUGXJHrHD1-0";

			Commands = new Dictionary<string, Command>();

			Commands["!colorme"] = ColorMe;
			Commands["!colorinfo"] = Info;
		}

		public override void Init(DiscordClient client)
		{
			client.ServerAvailable += (s, e) =>
			{
				Console.WriteLine("Colorbot connected.");
				//e.Server.FindChannels("general").First().SendMessage("Colorbot online.");
			};

			client.MessageReceived += MessageRecieved;
		}

		public void MessageRecieved(object s, MessageEventArgs e)
		{
			if (e.User.Id == ClientID)
				return;

			string body = e.Message.Text;

			if (!body.StartsWith("!"))
				return;

			List<string> tokens = body.Split(' ').ToList();
			string command = tokens[0];
			tokens = tokens.Skip(1).ToList();

			if (Commands.ContainsKey(command))
				Commands[command].Invoke(e.Channel, e.User, tokens);
			//e.Server.FindChannels("general").First().SendMessage("nuh uh: " + e.Message);
		}

		public void ColorMe(Channel channel, User caller, IEnumerable<string> tokens)
		{
			Server server = channel.Server;

			foreach (string s in tokens)
			{
				Role role = server.Roles.FirstOrDefault(x => x.Name == s);
				if(role != null)
				{
					if (role.Permissions.Equals(ServerPermissions.None))
					{
						caller.AddRoles(role);
						channel.SendMessage($"{caller.Mention} is now {role.Name}.");
						continue;
					}
				}

				if(s.StartsWith("#") && s.Length == 7)
				{

				}

				channel.SendMessage($"{caller.Mention}, {s} is not a valid color role.");

				
			}
				
		}

		//dark background:  #36393E
		//dark text      :  #BFC0C1
		//light background: #FFFFFF
		//light text     :  #2E3136

		//https://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef
		// should be at least 4.5:1
		public bool CheckRatios(string html)
		{
			return false;
		}

		//https://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
		public float RelativeLuminance(Color c1, Color c2)
		{
			return 0;
		}

		public void Info(Channel channel, User caller, IEnumerable<string> tokens)
		{
			//List<string> colors = channel.Server.Roles.Select(x => x.Permissions)
		}
	}
}
