﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RationalBot
{
	public abstract class RationalBot
	{
		public string Name = "RationalBot";
		public ulong ClientID = 0;
		public string ClientSecret = "";
		public string Token = "";

		public delegate void Command(Channel channel, User caller, IEnumerable<string> tokens);

		public Dictionary<string, Command> Commands;

		public abstract void Init(DiscordClient client);

		//https://discordapp.com/oauth2/authorize?&client_id=291645535599984651&scope=bot&permissions=0
	}
}
